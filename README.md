# FSFE discourse

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/community/00_README)

This Docker container is used for the FSFE Discourse discussion platform. It is
based on the [official Discourse
image](https://github.com/discourse/discourse_docker).

## Usage

The main configuration file is [community.yml](containers/community.yml). In
this file you can change all the discourse and docker configuration.

To deploy initially, run `ansible-playbook setup.yml`. Also use this if you
changed something regarding domains or webserver.

Please go to the [official
documentation](https://github.com/discourse/discourse/tree/master/docs) of the
discourse image.

## Upgrade

In order to upgrade this image, run the trimmed-down playbook:

`ansible-playbook upgrade.yml`

## License and Copyright

GNU GPL 2.0
